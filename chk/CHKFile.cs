﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriggerParser.chk {
    class CHKFile {

        BinaryReader reader;

        private class MyYupeExceptionThatIndicatesThatEndOfStreamIsReached : Exception {

        }

        List<Section> sections = new List<Section>();
        private bool valid;

        private CHKFile(string filePath) {
            reader = new BinaryReader(new FileStream(filePath,FileMode.Open));
            valid = parseSection();
            reader.Close();
        }

        private int lastChar;
        private bool hasLastChar = false;

        private int readTotal = 0;

        private int getNextChar() {
            if (hasLastChar) {
                hasLastChar = false;
            } else {
                reader.BaseStream.Seek(readTotal, SeekOrigin.Begin);
                readTotal ++;
                try {
                    lastChar = reader.ReadByte();
                } catch (Exception) {
                    lastChar = -1;
                }
            }
            if(lastChar == -1) {
                throw new MyYupeExceptionThatIndicatesThatEndOfStreamIsReached(); 
            }
            return lastChar;
        }

        private void unreadLastChar() {
            hasLastChar = true;
        }

        private string readString(int length) {
            StringBuilder sb = new StringBuilder();
            for(int i=0;i<length;i++) {
                int ci = getNextChar();
                sb.Append((char)ci);
            }
            return sb.ToString();
        }

        private char[] readArray(int length) {
            readTotal += length;
            char[] data = reader.ReadChars(length);
            return data;
        }

        private ushort readByte() {
            return (ushort) (getNextChar()&0xff);
        }

        private ushort readShort() {
            return (ushort)((readByte() ) | (readByte()) << 8);
        }

        private uint readInt() {
            return (uint)((readShort() ) | (readShort()) << 16);
        }

        private bool parseSection() {
            try {
                string name = readString(4);
                uint size = readInt();
                if (size > 10 * 1024 * 1024) { // 10 MB and above section indicates error
                    return false;
                }
                sections.Add(new chk.Section(name, size, readArray((int)size)));
                return parseSection();
            } catch (MyYupeExceptionThatIndicatesThatEndOfStreamIsReached) {
                return true;
            }
        }

        private bool isValid() {
            return valid;
        }

        private List<Section> getSections() {
            return sections;
        }

        public static List<Section> getSections(string filePath) {
            CHKFile f = new chk.CHKFile(filePath);
            return f.getSections();
        }
    }
}
