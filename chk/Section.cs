﻿
namespace TriggerParser.chk {
    class Section {

        private string name;
        private uint size;
        private char[] data;

        public string getName() {
            return name;
        }

        public uint getSize() {
            return size;
        }

        public override string ToString() {
            return "Section " + name;
        }

        public Section(string name, uint size, char[] data) {
            this.name = name;
            this.size = size;
            this.data = data;
        }

    }
}
