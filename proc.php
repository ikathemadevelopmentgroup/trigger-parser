<?php
$templateConditions = array (
		"Accumulate(PLAYER, LEASTMOST, COUNT, RESOURCES);",
		"Always();",
		"Bring(PLAYER, UNIT, LOC, LEASTMOST, COUNT);",
		"Command(PLAYER, UNIT, LEASTMOST, COUNT);",
		"Command the Least(UNIT);",
		"Command the Least At(UNIT, LOC);",
		"Command the Most(UNIT);",
		"Commands the Most At(UNIT, LOC);",
		"Countdown Timer(LEASTMOST, COUNT);",
		"Deaths(PLAYER, UNIT, LEASTMOST, COUNT);",
		"Elapsed Time(LEASTMOST, COUNT);",
		"Highest Score(LEADERBOARD);",
		"Kill(PLAYER, UNIT, LEASTMOST, COUNT);",
		"Least Kills(UNIT);",
		"Least Resources(RESOURCES);",
		"Lowest Score(LEADERBOARD);",
		"Most Kills(UNIT);",
		"Most Resources(RESOURCES);",
		"Never();",
		"Opponents(PLAYER, LEASTMOST, COUNT);",
		"Score(PLAYER, LEADERBOARD, LEASTMOST, COUNT);",
		"Switch(PLAYER, SWITCHSTATE);",
);

$templateActions = array (
		"Center View(LOC);",
		"Comment(TITLE);",
		"Create Unit(PLAYER, UNIT, COUNT, LOC);",
		"Create Unit with Properties(PLAYER, UNIT, COUNT, LOC, PROP);",
		"Defeat();",
		"Display Text Message(DISPLAY, MESSAGE);",
		"Draw();",
		"Give Units to Player(PLAYERFROM, PLAYERTO, UNIT, COUNT, LOC);",
		"Kill Unit(PLAYER, UNIT);",
		"Kill Unit At Location(PLAYER, UNIT, COUNT, LOC);",
		"Leader Board Control At Location(TITLE, UNIT, LOC);",
		"Leader Board Control(TITLE, UNIT);",
		"Leaderboard Greed(COUNT);",
		"Leader Board Kills(TITLE, UNIT);",
		"Leader Board Points(TITLE, LEADERBOARD);",
		"Leader Board Resources(TITLE, RESOURCES);",
		"Leaderboard Computer Players(ENDISTOG);",
		"Leaderboard Goal Control At Location(TITLE, UNIT, COUNT, LOC);",
		"Leaderboard Goal Control(TITLE, UNIT, COUNT);",
		"Leaderboard Goal Kills(TITLE, UNIT, COUNT);",
		"Leaderboard Goal Points(TITLE, LEADERBOARD, COUNT);",
		"Leaderboard Goal Resources(TITLE, COUNT, RESOURCES);",
		"Minimap Ping(LOC);",
		"Modify Unit Energy(PLAYER, UNIT, PERCENT, COUNT, LOC);",
		"Modify Unit Hanger Count(PLAYER, UNIT, HANGAR, COUNT, LOC);",
		"Modify Unit Hit Points(PLAYER, UNIT, HITPOINT, COUNT, LOC);",
		"Modify Unit Resource Amount(PLAYER, PERCENT, COUNT, LOC);",
		"Modify Unit Shield Points(PLAYER, UNIT, PERCENT, COUNT, LOC);",
		"Move Location(PLAYER, UNIT, LOCFROM, LOCTO);",
		"Play WAV(WAVEPATH, PARAM);",
		"Move Unit(PLAYER, UNIT, COUNT, LOCFROM, LOCTO);",
		"Mute Unit Speech();",
		"Order(PLAYER, UNIT, LOCFROM, LOCTO, ORDER);",
		"Pause Game();",
		"Pause Timer();",
		"Preserve Trigger();",
		"Remove Unit(PLAYER, UNIT);",
		"Remove Unit At Location(PLAYER, UNIT, COUNT, LOC);",
		"Run AI Script(SCRIPTNAME);",
		"Run AI Script At Location(SCRIPTNAME, LOC);",
		"Set Alliance Status(PLAYER, ALIANCE);",
		"Set Countdown Timer(SET, COUNT);",
		"Set Deaths(PLAYER, UNIT, SET, COUNT);",
		"Set Doodad State(PLAYER, UNIT, LOC, ENDISTOG);",
		"Set Invincibility(PLAYER, UNIT, LOC, ENDISTOG);",
		"Set Mission Objectives(OBJECTIVE);",
		"Set Next Scenario(NEXTSCENARIO);",
		"Set Resources(PLAYER, SET, COUNT, RESOURCES);",
		"Set Score(PLAYER, SET, COUNT, LEADERBOARD);",
		"Set Switch(SWITCHNAME, SETCLEARTOGGLERAND);",
		"Talking Portrait(UNIT, TIMEOUT);",
		"Transmission(DISPLAY, TITLE, UNIT, LOC, SET, COUNT, WAVEPATH, DURATION);",
		"Unmute Unit Speech();",
		"Unpause Game();",
		"Unpause Timer();",
		"Victory();",
		"Wait(TIMEOUT);" 
);

$dataDescriptors = array (
		"SWITCHSTATE" =>array("SwitchState","SwitchState"),
		"LEASTMOST" => array("Quantifier","Quantifier"),
		"PLAYER" => array("PlayerDef","Player"),
		"PLAYERFROM" => array("PlayerDef","SourcePlayer"),
		"PLAYERTO" => array("PlayerDef","TargetPlayer"),
		"UNIT" => array("string","UnitName"),
		"OBJECTIVE" => array("string","NewObjective"),
		"LEADERBOARD" => array("ScoreBoard","ScoreBoard"),
		"ENDISTOG" => array("EnableState","State"),
		"DISPLAY" => array("MessageType","MessageType"),
		"ALIANCE" => array("Alliance","Alliance"),
		"SETCLEARTOGGLERAND" => array("SwitchSetState","SwitchSetState"),
		"NEXTSCENARIO" => array("string","NextScenario"),
		"LOC" => array("string","Location"),
		"LOCFROM" => array("string","SourceLocation"),
		"LOCTO" => array("string","TargetLocation"),
		"TITLE" => array("string","Title"),
		"COUNT" => array("int","Amount"),
		"RESOURCES" => array("Resources","Resources"),
		"MESSAGE" => array("string","Message"),
		"PERCENT" => array("int","Percentage"),
		"ORDER" => array("Order","OrderType"),
		"DURATION" => array("int","Duration"),
		"PROP" => array("int","Properties"),
		"HANGAR" => array("int","NewHangarCount"),
		"HITPOINT" => array("int","NewHitPoint"),
		"PARAM" => array("string","Parameter"),
		"TIMEOUT" => array("int","Timeout"),
		"SWITCHNAME" => array("string","SwitchName"),
		"SET" => array("SetQuantifier","SetQuantifier"),
		"SCRIPTNAME" => array("string","ScriptName"),
		"WAVEPATH" => array("string","WavPath"),
);


function getClassDefinitions($prefix, $rawData, $dataDescriptors) {
	
	$resultArray=array();
	$alreadyProcessedNames=array();
	
	foreach ( $rawData as $rawDataItem ) {
		
		// Get name of item
		$name = explode ( "(", $rawDataItem );
		$name = $name [0];
		$nameByWords = explode ( " ", $name );
		if (count ( $nameByWords ) > 1) {
			$name="";
			foreach ( $nameByWords as $wordInName ) {
				$name .= ucfirst ( $wordInName );
			}
		}
		$name = ucfirst ( $name );
		
		// Remove duplicated items
		if (isset ( $alreadyProcessedNames [$name] )) {
			continue;
		} else {
			$alreadyProcessedNames [$name] = true;
		}
		
		// Detect number of parameters
		if (substr_count ( $rawDataItem, "()" ) == 1) {
			$parametersCount = 0;
		} else {
			$parametersCount = substr_count ( $rawDataItem, "," ) + 1;
		}
	
		// Function definition, first part
		$resultArray[] = "	class $prefix$name : $prefix {";
		$resultArray[] = "";


		$getterFunctionsArray=array();
		$getterFunctionsDataArray=array();
		
		// If there are parameters, extract those parameters
		if ($parametersCount > 0) {
			// Extract parameters
			$parametersArray = explode ( ",", substr($rawDataItem, strpos($rawDataItem,"(")+1, strpos($rawDataItem,")") - strpos($rawDataItem,"(")-1));
			foreach ( $parametersArray as $parameterIndex => $parameterName) {
				$parameterName = trim ( $parameterName );
				$realIndex = $parameterIndex + 1;
				if(isset($dataDescriptors[$parameterName])) {
					$dataDescriptor=$dataDescriptors[$parameterName];
					$parameterDataType=$dataDescriptor[0];
					$parameterFunctionName=$dataDescriptor[1];
					$parameterVariableName=lcfirst($parameterFunctionName);
					$getterFunctionsArray[] = "";
					$getterFunctionsArray[] = "\t\tpublic $parameterDataType get$parameterFunctionName() {";
					$getterFunctionsArray[] = "\t\t\treturn this.$parameterVariableName;";
					$getterFunctionsArray[] = "\t\t}";
					$getterFunctionsDataArray[]=array($dataDescriptor,$realIndex);
					
				} else {
					echo "Unknown parameter: ".$parameterName."\r\n";
				}
			}
		}
		foreach($getterFunctionsDataArray as $getterFunctionsData) {
			$dataType=$getterFunctionsData[0][0];
			$dataName=lcfirst($getterFunctionsData[0][1]);
			$realIndex=$getterFunctionsData[1];
			$resultArray[] = "\t\tprivate $dataType $dataName;";
			$resultArray[] = "";
		}
		$resultArray[] = "\t\tpublic $prefix$name(Parser parser) : base(parser, $parametersCount) {";
		foreach($getterFunctionsDataArray as $getterFunctionsData) {
			$dataType=ucfirst($getterFunctionsData[0][0]);
			$dataName=lcfirst($getterFunctionsData[0][1]);
			$realIndex=$getterFunctionsData[1];
			$resultArray[] = "\t\t\tthis.$dataName = base.get$dataType($realIndex);";
		}
			
		$resultArray[] = "\t\t}";
		foreach($getterFunctionsArray as $getterFunction) {
			$resultArray[] = $getterFunction;
		}
		$resultArray[] = "\t}";
		$resultArray[] = "";
		
	}
	return $resultArray;
}

function getComparators($prefix, $rawData) {
	$resultArray=array();
	$alreadyProcessedNames=array();
	
	foreach ( $rawData as $rawDataItem ) {
	
		// Get name of item
		$name = explode ( "(", $rawDataItem );
		$name = $name [0];
		$nameByWords = explode ( " ", $name );
		if (count ( $nameByWords ) > 1) {
			$name="";
			foreach ( $nameByWords as $wordInName ) {
				$name .= ucfirst ( $wordInName );
			}
		}
		$name = ucfirst ( $name );
	
		// Remove duplicated items
		if (isset ( $alreadyProcessedNames [$name] )) {
			continue;
		} else {
			$alreadyProcessedNames [$name] = true;
		}
		$name=ucfirst($name);
		
		$resultArray[] = "if (ct.is$name"."()) {\r\n\t\t\t\treturn new $prefix$name"."(this);\r\n\t\t\t}";
	}
	return "\t\t\t".implode(" else ",$resultArray);
}

$actions=array($templateActions,"Action",$dataDescriptors);
$conditions=array($templateConditions,"Condition",$dataDescriptors);

function saveClassDefinition($data) {
	$actions=getClassDefinitions($data[1], $data[0], $data[2]);
	$text = implode ( "\r\n", $actions );
	file_put_contents ( "out.txt", $text );
}

function saveConditions($templateConditions, $dataDescriptors) {
	$conditions=getClassDefinitions("Condition", $templateConditions, $dataDescriptors);
	$text = implode ( "\r\n", $conditions );
	file_put_contents ( "out.txt", $text );
}

function saveParserComparators($data) {
	$conditions=getComparators($data[1], $data[0]);
	$text=$conditions;
	file_put_contents ( "out.txt", $text );
}

//saveClassDefinition($actions);
//saveClassDefinition($conditions);


//saveParserComparators($actions);
saveParserComparators($conditions);