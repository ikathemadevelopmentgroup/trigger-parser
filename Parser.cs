﻿using System;
using System.Collections.Generic;
using TriggerParser.triggers;
using TriggerParser.triggers.actions;
using TriggerParser.triggers.conditions;

namespace TriggerParser {
    class Parser {
        private Scanner scanner;

        public Parser(Scanner s) {
            this.scanner = s;
        }

        public void close() {
            scanner.close();
        }

        public bool parse() {
            allTriggers = new List<Trigger>();
            return parseTokens();
        }

        public List<Trigger> getTriggers() {
            return allTriggers;
        }

        public Token getNextToken() {
            Token lastToken = scanner.getNextToken();
            currentTrigger.addToken(lastToken);
            return lastToken;
        }

        private List<Trigger> allTriggers = new List<Trigger>();

        private Trigger currentTrigger;

        private bool parseTokens() {
            currentTrigger = new Trigger();
            Token t = getNextToken();
            if(t==null) {
                return true;
            } else {
                if(t is CommandToken) {
                    CommandToken ct = t as CommandToken;
                    if(ct.isTrigger()) {
                        if (getNextToken() is LeftBracket) {
                            Token affectName = getNextToken();
                            currentTrigger.setAffectName(affectName);
                            allTriggers.Add(currentTrigger);
                            if (affectName is StringToken) {
                                string triggerOwner = affectName.getContent();
                                if(getNextToken() is RightBracket) {
                                    if(getNextToken() is StartBracket) {
                                        t = getNextToken();
                                        if (t is CommandToken) {
                                            ct = t as CommandToken;
                                            if (ct.isConditions()) {
                                                if(getNextToken() is Colon) { 
                                                   if (parseConditions()) {
                                                        if (parseActions()) {
                                                            if (getNextToken() is TokenEnd) {
                                                                return parseTokens();
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return false;
            }
        }

        private Condition GetCondition(CommandToken ct) {
            if (ct.isAccumulate()) {
                return new ConditionAccumulate(this);
            } else if (ct.isAlways()) {
                return new ConditionAlways(this);
            } else if (ct.isBring()) {
                return new ConditionBring(this);
            } else if (ct.isCommand()) {
                return new ConditionCommand(this);
            } else if (ct.isCommandTheLeast()) {
                return new ConditionCommandTheLeast(this);
            } else if (ct.isCommandTheLeastAt()) {
                return new ConditionCommandTheLeastAt(this);
            } else if (ct.isCommandTheMost()) {
                return new ConditionCommandTheMost(this);
            } else if (ct.isCommandsTheMostAt()) {
                return new ConditionCommandsTheMostAt(this);
            } else if (ct.isCountdownTimer()) {
                return new ConditionCountdownTimer(this);
            } else if (ct.isDeaths()) {
                return new ConditionDeaths(this);
            } else if (ct.isElapsedTime()) {
                return new ConditionElapsedTime(this);
            } else if (ct.isHighestScore()) {
                return new ConditionHighestScore(this);
            } else if (ct.isKill()) {
                return new ConditionKill(this);
            } else if (ct.isLeastKills()) {
                return new ConditionLeastKills(this);
            } else if (ct.isLeastResources()) {
                return new ConditionLeastResources(this);
            } else if (ct.isLowestScore()) {
                return new ConditionLowestScore(this);
            } else if (ct.isMostKills()) {
                return new ConditionMostKills(this);
            } else if (ct.isMostResources()) {
                return new ConditionMostResources(this);
            } else if (ct.isNever()) {
                return new ConditionNever(this);
            } else if (ct.isOpponents()) {
                return new ConditionOpponents(this);
            } else if (ct.isScore()) {
                return new ConditionScore(this);
            } else if (ct.isSwitch()) {
                return new ConditionSwitch(this);
            }
            throw new NotImplementedException();
        }

        private triggers.actions.Action getAction(CommandToken ct) {
            if (ct.isCenterView()) {
                return new ActionCenterView(this);
            } else if (ct.isComment()) {
                return new ActionComment(this);
            } else if (ct.isCreateUnit()) {
                return new ActionCreateUnit(this);
            } else if (ct.isCreateUnitWithProperties()) {
                return new ActionCreateUnitWithProperties(this);
            } else if (ct.isDefeat()) {
                return new ActionDefeat(this);
            } else if (ct.isDisplayTextMessage()) {
                return new ActionDisplayTextMessage(this);
            } else if (ct.isDraw()) {
                return new ActionDraw(this);
            } else if (ct.isGiveUnitsToPlayer()) {
                return new ActionGiveUnitsToPlayer(this);
            } else if (ct.isKillUnit()) {
                return new ActionKillUnit(this);
            } else if (ct.isKillUnitAtLocation()) {
                return new ActionKillUnitAtLocation(this);
            } else if (ct.isLeaderBoardControlAtLocation()) {
                return new ActionLeaderBoardControlAtLocation(this);
            } else if (ct.isLeaderBoardControl()) {
                return new ActionLeaderBoardControl(this);
            } else if (ct.isLeaderboardGreed()) {
                return new ActionLeaderboardGreed(this);
            } else if (ct.isLeaderBoardKills()) {
                return new ActionLeaderBoardKills(this);
            } else if (ct.isLeaderBoardPoints()) {
                return new ActionLeaderBoardPoints(this);
            } else if (ct.isLeaderBoardResources()) {
                return new ActionLeaderBoardResources(this);
            } else if (ct.isLeaderboardComputerPlayers()) {
                return new ActionLeaderboardComputerPlayers(this);
            } else if (ct.isLeaderboardGoalControlAtLocation()) {
                return new ActionLeaderboardGoalControlAtLocation(this);
            } else if (ct.isLeaderboardGoalControl()) {
                return new ActionLeaderboardGoalControl(this);
            } else if (ct.isLeaderboardGoalKills()) {
                return new ActionLeaderboardGoalKills(this);
            } else if (ct.isLeaderboardGoalPoints()) {
                return new ActionLeaderboardGoalPoints(this);
            } else if (ct.isLeaderboardGoalResources()) {
                return new ActionLeaderboardGoalResources(this);
            } else if (ct.isMinimapPing()) {
                return new ActionMinimapPing(this);
            } else if (ct.isModifyUnitEnergy()) {
                return new ActionModifyUnitEnergy(this);
            } else if (ct.isModifyUnitHangerCount()) {
                return new ActionModifyUnitHangerCount(this);
            } else if (ct.isModifyUnitHitPoints()) {
                return new ActionModifyUnitHitPoints(this);
            } else if (ct.isModifyUnitResourceAmount()) {
                return new ActionModifyUnitResourceAmount(this);
            } else if (ct.isModifyUnitShieldPoints()) {
                return new ActionModifyUnitShieldPoints(this);
            } else if (ct.isMoveLocation()) {
                return new ActionMoveLocation(this);
            } else if (ct.isPlayWAV()) {
                return new ActionPlayWAV(this);
            } else if (ct.isMoveUnit()) {
                return new ActionMoveUnit(this);
            } else if (ct.isMuteUnitSpeech()) {
                return new ActionMuteUnitSpeech(this);
            } else if (ct.isOrder()) {
                return new ActionOrder(this);
            } else if (ct.isPauseGame()) {
                return new ActionPauseGame(this);
            } else if (ct.isPauseTimer()) {
                return new ActionPauseTimer(this);
            } else if (ct.isPreserveTrigger()) {
                return new ActionPreserveTrigger(this);
            } else if (ct.isRemoveUnit()) {
                return new ActionRemoveUnit(this);
            } else if (ct.isRemoveUnitAtLocation()) {
                return new ActionRemoveUnitAtLocation(this);
            } else if (ct.isRunAIScript()) {
                return new ActionRunAIScript(this);
            } else if (ct.isRunAIScriptAtLocation()) {
                return new ActionRunAIScriptAtLocation(this);
            } else if (ct.isSetAllianceStatus()) {
                return new ActionSetAllianceStatus(this);
            } else if (ct.isSetCountdownTimer()) {
                return new ActionSetCountdownTimer(this);
            } else if (ct.isSetDeaths()) {
                return new ActionSetDeaths(this);
            } else if (ct.isSetDoodadState()) {
                return new ActionSetDoodadState(this);
            } else if (ct.isSetInvincibility()) {
                return new ActionSetInvincibility(this);
            } else if (ct.isSetMissionObjectives()) {
                return new ActionSetMissionObjectives(this);
            } else if (ct.isSetNextScenario()) {
                return new ActionSetNextScenario(this);
            } else if (ct.isSetResources()) {
                return new ActionSetResources(this);
            } else if (ct.isSetScore()) {
                return new ActionSetScore(this);
            } else if (ct.isSetSwitch()) {
                return new ActionSetSwitch(this);
            } else if (ct.isTalkingPortrait()) {
                return new ActionTalkingPortrait(this);
            } else if (ct.isTransmission()) {
                return new ActionTransmission(this);
            } else if (ct.isUnmuteUnitSpeech()) {
                return new ActionUnmuteUnitSpeech(this);
            } else if (ct.isUnpauseGame()) {
                return new ActionUnpauseGame(this);
            } else if (ct.isUnpauseTimer()) {
                return new ActionUnpauseTimer(this);
            } else if (ct.isVictory()) {
                return new ActionVictory(this);
            } else if (ct.isWait()) {
                return new ActionWait(this);
            }
            throw new NotImplementedException();
        }

        private bool parseConditions() {
            Token t = getNextToken();
            bool commented = false;
            if (t is Semicolon) { // Comment
                commented = true;
                t = getNextToken();
            }
            if (t is CommandToken) {
                CommandToken ct = t as CommandToken;
                if(ct.isActions()) {
                    if(!commented) {
                        if (getNextToken() is Colon) {
                            return true;
                        }
                    }
                } else {
                    Condition c = GetCondition(ct);
                    c.setEnabled(!commented);
                    currentTrigger.addCondition(c);
                    t = getNextToken();
                    if (t is Semicolon) {
                        return parseConditions();
                    }
                }
            }
            return false;
        }

        private bool parseActions() {
            Token t = getNextToken();
            bool commented = false;
            if(t is Semicolon) { // Comment
                commented = true;
                t = getNextToken();
            } else {
                if(t is EndBracket) {
                    return true;
                }
            }

            if (t is CommandToken) {
                CommandToken ct = t as CommandToken;
                triggers.actions.Action a = getAction(ct);
                a.setEnabled(!commented);
                currentTrigger.addAction(a);
                t = getNextToken();
                if (t is Semicolon) {
                    return parseActions();
                }
            }
            return false;
        }
    }
}
