﻿using System;
using TriggerParser.triggers;

namespace TriggerParser {

    class Token {

        private string content;

        public Token(string content) {
            this.content = content;
        }

        public string getContent() {
            return content;
        }

        private bool iss(string s) {
            return content.ToLower().Equals(s.ToLower());
        }

        public int toInt() {
            if(this is NumToken) {
                return Int32.Parse(content);
            } else if(this is CommandToken) {
                CommandToken ct = this as CommandToken;
                if(ct.isAll()) {
                    return -1;
                }
            }
            throw new NotImplementedException();
        }

        public EnableState toEnableState() {
            if (this is CommandToken) {
                CommandToken ct = this as CommandToken;
                if (ct.isDisabled()) {
                    return EnableState.Disable;
                } else if (ct.isEnabled()) {
                    return EnableState.Enable;
                } else if (ct.isToggle()) {
                    return EnableState.Toggle;
                }
            }
            throw new NotImplementedException();
        }

        public SwitchSetState toSwitchSetState() {
            if (this is CommandToken) {
                CommandToken ct = this as CommandToken;
                if (ct.isClear()) {
                    return SwitchSetState.Clear;
                } else if (ct.isRandomize()) {
                    return SwitchSetState.Randomize;
                } else if (ct.isSet()) {
                    return SwitchSetState.Set;
                } else if (ct.isToggle()) {
                    return SwitchSetState.Toggle;
                }
            }
            throw new NotImplementedException();
        }

        public Resources toResources() {
            if(this is CommandToken) {
                CommandToken ct = this as CommandToken;
                if(ct.isOre()) {
                    return Resources.Ore;
                } else if(ct.isGas()) {
                    return Resources.Gas;
                } else if(ct.isOreAndGas()) {
                    return Resources.OreAndGas;
                }
            }
            throw new NotImplementedException();
        }


        public Alliance toAlliance() {
            if (this is CommandToken) {
                CommandToken ct = this as CommandToken;
                if (ct.isAlliedVictory()) {
                    return Alliance.AlliedVictory;
                } else if (ct.isAlly()) {
                    return Alliance.Ally;
                } else if (ct.isEnemy()) {
                    return Alliance.Enemy;
                }
            }
            throw new NotImplementedException();
        }

        public MessageType getMessageType() {
            if (this is CommandToken) {
                CommandToken ct = this as CommandToken;
                if (ct.isAlwaysDisplay()) {
                    return MessageType.ALwaysDisplay;
                } else if (ct.isDontAlwaysDisplay()) {
                    return MessageType.DontAlwaysDisplay;
                }
            }
            throw new NotImplementedException();
        }

        public Order toOrder() {
            if (this is CommandToken) {
                CommandToken ct = this as CommandToken;
                if (ct.isMove()) {
                    return Order.Move;
                } else if (ct.isAttack()) {
                    return Order.Attack;
                } else if (ct.isPatrol()) {
                    return Order.Patrol;
                }
            }
            throw new NotImplementedException();
        }

        public SetQuantifier toSetQuantifier() {
            if (this is CommandToken) {
                CommandToken ct = this as CommandToken;
                if (ct.isAdd()) {
                    return SetQuantifier.Add;
                } else if (ct.isSubtract()) {
                    return SetQuantifier.Subtract;
                } else if (ct.isSetTo()) {
                    return SetQuantifier.SetTo;
                }
            }
            throw new NotImplementedException();
        }

        public Quantifier toQuantifier() {
            if(this is CommandToken) {
                CommandToken ct = this as CommandToken;
                if(ct.isAtLeast()) {
                    return Quantifier.AtLeast;
                } else if(ct.isAtMost()) {
                    return Quantifier.AtMost;
                } else if(ct.isExactly()) {
                    return Quantifier.Exactly;
                }
            }
            throw new NotImplementedException();
        }

        public PlayerDef toPlayerDef() {
            if (this is StringToken) {
                if (iss("Player 1")) {
                    return PlayerDef.Player1;
                } else if (iss("Player 2")) {
                    return PlayerDef.Player2;
                } else if (iss("Player 3")) {
                    return PlayerDef.Player3;
                } else if (iss("Player 4")) {
                    return PlayerDef.Player4;
                } else if (iss("Player 5")) {
                    return PlayerDef.Player5;
                } else if (iss("Player 6")) {
                    return PlayerDef.Player6;
                } else if (iss("Player 7")) {
                    return PlayerDef.Player7;
                } else if (iss("Player 8")) {
                    return PlayerDef.Player8;
                } else if (iss("Player 9")) {
                    return PlayerDef.Player9;
                } else if (iss("Player 10")) {
                    return PlayerDef.Player10;
                } else if (iss("Player 11")) {
                    return PlayerDef.Player11;
                } else if (iss("All Players")) {
                    return PlayerDef.AllPlayers;
                } else if (iss("Allies")) {
                    return PlayerDef.Allies;
                } else if (iss("Foes")) {
                    return PlayerDef.Foes;
                } else if (iss("Neutral")) {
                    return PlayerDef.AllPlayers;
                } else if (iss("Neutral Players")) {
                    return PlayerDef.AllPlayers;
                } else if (iss("Non Allied Victory Players")) {
                    return PlayerDef.NonAlliedVictoryPlayers;
                }
            }
            throw new NotImplementedException();
        }

        public ScoreBoard toScoreBoard() {
            if(this is CommandToken) {
                CommandToken ct = this as CommandToken;
                if(ct.isBuildings()) {
                    return ScoreBoard.Buildings;
                } else if (ct.isCustom()) {
                    return ScoreBoard.Custom;
                } else if (ct.isKills()) {
                    return ScoreBoard.Kills;
                } else if (ct.isKillsAndRazings()) {
                    return ScoreBoard.KillsAndRazings;
                } else if (ct.isRazings()) {
                    return ScoreBoard.Razings;
                } else if (ct.isTotal()) {
                    return ScoreBoard.Total;
                } else if (ct.isUnits()) {
                    return ScoreBoard.Units;
                } else if (ct.isUnitsAndBuildings()) {
                    return ScoreBoard.UnitsAndBuildings;
                }
            }
            throw new NotImplementedException();
        }

        public SwitchState toSwitchState() {
            if (this is CommandToken) {
                CommandToken ct = this as CommandToken;
                if(ct.isSet()) {
                    return SwitchState.Set;
                } else if(ct.isNotSet()) {
                    return SwitchState.NotSet;
                }
            }
            throw new NotImplementedException();
        }
    }

    class LeftBracket : Token { public LeftBracket() : base("(") { } }

    class RightBracket : Token { public RightBracket() : base(")") { } }

    class Semicolon : Token { public Semicolon() : base(";") { } }

    class Comma : Token { public Comma() : base(",") { } }

    class Dot : Token { public Dot() : base(".") { } }

    class Colon : Token { public Colon() : base(":") { } }

    class StartBracket : Token { public StartBracket() : base("{") { } }

    class EndBracket : Token { public EndBracket() : base("}") { } }


    class StringToken : Token { public StringToken(string s) : base(s) { } }

    class NumToken : Token { public NumToken(string s) : base(s) { } }

    class TokenEnd : Token { public TokenEnd() : base("") { } }

    class CommandToken : Token {
        public CommandToken(string command) : base(command) {

        }

        public static readonly string[] specials = new string[] {
            "Trigger",
            "Conditions",
            "Actions",
        };

        public static readonly string[] conditions = new string[] {
            "Accumulate",
            "Always",
            "Bring",
            "Command",
            "Command the Least",
            "Command the Least At",
            "Command the Most",
            "Commands the Most At",
            "Countdown Timer",
            "Deaths",
            "Elapsed Time",
            "Highest Score",
            "Kill",
            "Least Kills",
            "Least Resources",
            "Lowest Score",
            "Most Kills",
            "Most Resources",
            "Never",
            "Opponents",
            "Score",
            "Switch",
        };

        public static readonly string[] actions = new string[] {
            "Center View",
            "Comment",
            "Create Unit",
            "Create Unit with Properties",
            "Defeat",
            "Display Text Message",
            "Draw",
            "Give Units to Player",
            "Kill Unit",
            "Kill Unit At Location",
            "Leader Board Control At Location",
            "Leader Board Control",
            "Leaderboard Greed",
            "Leader Board Kills",
            "Leader Board Points",
            "Leader Board Resources",
            "Leaderboard Computer Players",
            "Leaderboard Goal Control At Location",
            "Leaderboard Goal Control",
            "Leaderboard Goal Kills",
            "Leaderboard Goal Points",
            "Leaderboard Goal Resources",
            "Minimap Ping",
            "Modify Unit Energy",
            "Modify Unit Hanger Count",
            "Modify Unit Hit Points",
            "Modify Unit Resource Amount",
            "Modify Unit Shield Points",
            "Move Location",
            "Play WAV",
            "Move Unit",
            "Mute Unit Speech",
            "Order",
            "Pause Game",
            "Pause Timer",
            "Preserve Trigger",
            "Remove Unit",
            "Remove Unit At Location",
            "Run AI Script",
            "Run AI Script At Location",
            "Set Alliance Status",
            "Set Countdown Timer",
            "Set Deaths",
            "Set Doodad State",
            "Set Invincibility",
            "Set Mission Objectives",
            "Set Next Scenario",
            "Set Resources",
            "Set Score",
            "Set Switch",
            "Talking Portrait",
            "Transmission",
            "Unmute Unit Speech",
            "Unpause Game",
            "Unpause Timer",
            "Victory",
            "Wait",

        };

        public static readonly string[] parameters = new string[] {
            "At most",
            "At least",
            "Exactly",
            "ore",
            "gas",
            "ore and gas",
            "patrol",
            "add",
            "subtract",
            "set to",
            "set",
            "clear",
            "randomize",
            "not set",
            "buildings",
            "custom",
            "kills",
            "kills and razings",
            "razings",
            "total",
            "units",
            "units and buildings",
            "All",
            "enabled",
            "disabled",
            "Toggle",
            "attack",
            "move",
            "patrol",
            "Ally",
            "Enemy",
            "Allied Victory",
            "Don't Always Display",
            "Always Display",
        };

        private bool isSomething(string what) {
            return base.getContent().ToLower().Equals(what.ToLower());
        }

        public bool isOneSpecial() {
            string f = base.getContent().ToLower();
            foreach (string special in specials) {
                string c = special.ToLower();
                if (f.Equals(c)) {
                    return true;
                }
            }
            return false;
        }

        public bool isOneParameter() {
            string f = base.getContent().ToLower();
            foreach (string parameter in parameters) {
                string c = parameter.ToLower();
                if (f.Equals(c)) {
                    return true;
                }
            }
            return false;
        }

        public bool isOneCondition() {
            string f = base.getContent().ToLower();
            foreach (string condition in conditions) {
                string c = condition.ToLower();
                if (f.Equals(c)) {
                    return true;
                }
            }
            return false;
        }

        public bool isOneAction() {
            string f = base.getContent().ToLower();
            foreach (string action in actions) {
                string c = action.ToLower();
                if (f.Equals(c)) {
                    return true;
                }
            }
            return false;
        }

        public bool isValid() {
            return isOneAction() || isOneCondition() || isOneSpecial() || isOneParameter();

        }

        public bool isAccumulate() {
            return isSomething("Accumulate");
        }

        public bool isActions() {
            return isSomething("Actions");
        }

        public bool isAdd() {
            return isSomething("Add");
        }

        public bool isAll() {
            return isSomething("All");
        }

        public bool isAlliedVictory() {
            return isSomething("Allied Victory");
        }

        public bool isAlly() {
            return isSomething("Ally");
        }

        public bool isAlways() {
            return isSomething("Always");
        }

        public bool isAlwaysDisplay() {
            return isSomething("Always Display");
        }

        public bool isAtLeast() {
            return isSomething("At Least");
        }

        public bool isAtMost() {
            return isSomething("At Most");
        }

        public bool isAttack() {
            return isSomething("Attack");
        }

        public bool isBring() {
            return isSomething("Bring");
        }

        public bool isBuildings() {
            return isSomething("Buildings");
        }

        public bool isCenterView() {
            return isSomething("Center View");
        }

        public bool isCommand() {
            return isSomething("Command");
        }

        public bool isCommandsTheMostAt() {
            return isSomething("Commands The Most At");
        }

        public bool isCommandTheLeast() {
            return isSomething("Command The Least");
        }

        public bool isCommandTheLeastAt() {
            return isSomething("Command The Least At");
        }

        public bool isCommandTheMost() {
            return isSomething("Command The Most");
        }

        public bool isComment() {
            return isSomething("Comment");
        }

        public bool isConditions() {
            return isSomething("Conditions");
        }

        public bool isCountdownTimer() {
            return isSomething("Countdown Timer");
        }

        public bool isCreateUnit() {
            return isSomething("Create Unit");
        }

        public bool isCreateUnitWithProperties() {
            return isSomething("Create Unit With Properties");
        }

        public bool isCustom() {
            return isSomething("Custom");
        }

        public bool isClear() {
            return isSomething("Clear");
        }

        public bool isDeaths() {
            return isSomething("Deaths");
        }

        public bool isDefeat() {
            return isSomething("Defeat");
        }

        public bool isDisabled() {
            return isSomething("Disabled");
        }

        public bool isDisplayTextMessage() {
            return isSomething("Display Text Message");
        }

        public bool isDontAlwaysDisplay() {
            return isSomething("Don't Always Display");
        }

        public bool isDraw() {
            return isSomething("Draw");
        }

        public bool isElapsedTime() {
            return isSomething("Elapsed Time");
        }

        public bool isEnabled() {
            return isSomething("Enabled");
        }

        public bool isEnemy() {
            return isSomething("Enemy");
        }

        public bool isExactly() {
            return isSomething("Exactly");
        }

        public bool isGas() {
            return isSomething("Gas");
        }

        public bool isGiveUnitsToPlayer() {
            return isSomething("Give Units To Player");
        }

        public bool isHighestScore() {
            return isSomething("Highest Score");
        }

        public bool isKill() {
            return isSomething("Kill");
        }

        public bool isKills() {
            return isSomething("Kills");
        }

        public bool isKillsAndRazings() {
            return isSomething("Kills And Razings");
        }

        public bool isKillUnit() {
            return isSomething("Kill Unit");
        }

        public bool isKillUnitAtLocation() {
            return isSomething("Kill Unit At Location");
        }

        public bool isLeaderboardComputerPlayers() {
            return isSomething("Leaderboard Computer Players");
        }

        public bool isLeaderBoardControl() {
            return isSomething("Leader Board Control");
        }

        public bool isLeaderBoardControlAtLocation() {
            return isSomething("Leader Board Control At Location");
        }

        public bool isLeaderboardGoalControl() {
            return isSomething("Leaderboard Goal Control");
        }

        public bool isLeaderboardGoalControlAtLocation() {
            return isSomething("Leaderboard Goal Control At Location");
        }

        public bool isLeaderboardGoalKills() {
            return isSomething("Leaderboard Goal Kills");
        }

        public bool isLeaderboardGoalPoints() {
            return isSomething("Leaderboard Goal Points");
        }

        public bool isLeaderboardGoalResources() {
            return isSomething("Leaderboard Goal Resources");
        }

        public bool isLeaderboardGreed() {
            return isSomething("Leaderboard Greed");
        }

        public bool isLeaderBoardKills() {
            return isSomething("Leader Board Kills");
        }

        public bool isLeaderBoardPoints() {
            return isSomething("Leader Board Points");
        }

        public bool isLeaderBoardResources() {
            return isSomething("Leader Board Resources");
        }

        public bool isLeastKills() {
            return isSomething("Least Kills");
        }

        public bool isLeastResources() {
            return isSomething("Least Resources");
        }

        public bool isLowestScore() {
            return isSomething("Lowest Score");
        }

        public bool isMinimapPing() {
            return isSomething("Minimap Ping");
        }

        public bool isModifyUnitEnergy() {
            return isSomething("Modify Unit Energy");
        }

        public bool isModifyUnitHangerCount() {
            return isSomething("Modify Unit Hanger Count");
        }

        public bool isModifyUnitHitPoints() {
            return isSomething("Modify Unit Hit Points");
        }

        public bool isModifyUnitResourceAmount() {
            return isSomething("Modify Unit Resource Amount");
        }

        public bool isModifyUnitShieldPoints() {
            return isSomething("Modify Unit Shield Points");
        }

        public bool isMostKills() {
            return isSomething("Most Kills");
        }

        public bool isMostResources() {
            return isSomething("Most Resources");
        }

        public bool isMove() {
            return isSomething("Move");
        }

        public bool isMoveLocation() {
            return isSomething("Move Location");
        }

        public bool isMoveUnit() {
            return isSomething("Move Unit");
        }

        public bool isMuteUnitSpeech() {
            return isSomething("Mute Unit Speech");
        }

        public bool isNever() {
            return isSomething("Never");
        }

        public bool isNotSet() {
            return isSomething("Not Set");
        }

        public bool isOpponents() {
            return isSomething("Opponents");
        }

        public bool isOrder() {
            return isSomething("Order");
        }

        public bool isOre() {
            return isSomething("Ore");
        }

        public bool isOreAndGas() {
            return isSomething("Ore And Gas");
        }

        public bool isPatrol() {
            return isSomething("Patrol");
        }

        public bool isPauseGame() {
            return isSomething("Pause Game");
        }

        public bool isPauseTimer() {
            return isSomething("Pause Timer");
        }

        public bool isPlayWAV() {
            return isSomething("Play WAV");
        }

        public bool isPreserveTrigger() {
            return isSomething("Preserve Trigger");
        }

        public bool isRandomize() {
            return isSomething("Randomize");
        }

        public bool isRazings() {
            return isSomething("Razings");
        }

        public bool isRemoveUnit() {
            return isSomething("Remove Unit");
        }

        public bool isRemoveUnitAtLocation() {
            return isSomething("Remove Unit At Location");
        }

        public bool isRunAIScript() {
            return isSomething("Run AI Script");
        }

        public bool isRunAIScriptAtLocation() {
            return isSomething("Run AI Script At Location");
        }

        public bool isScore() {
            return isSomething("Score");
        }

        public bool isSet() {
            return isSomething("Set");
        }

        public bool isSetAllianceStatus() {
            return isSomething("Set Alliance Status");
        }

        public bool isSetCountdownTimer() {
            return isSomething("Set Countdown Timer");
        }

        public bool isSetDeaths() {
            return isSomething("Set Deaths");
        }

        public bool isSetDoodadState() {
            return isSomething("Set Doodad State");
        }

        public bool isSetInvincibility() {
            return isSomething("Set Invincibility");
        }

        public bool isSetMissionObjectives() {
            return isSomething("Set Mission Objectives");
        }

        public bool isSetNextScenario() {
            return isSomething("Set Next Scenario");
        }

        public bool isSetResources() {
            return isSomething("Set Resources");
        }

        public bool isSetScore() {
            return isSomething("Set Score");
        }

        public bool isSetSwitch() {
            return isSomething("Set Switch");
        }

        public bool isSetTo() {
            return isSomething("Set To");
        }

        public bool isSubtract() {
            return isSomething("Subtract");
        }

        public bool isSwitch() {
            return isSomething("Switch");
        }

        public bool isTalkingPortrait() {
            return isSomething("Talking Portrait");
        }

        public bool isToggle() {
            return isSomething("Toggle");
        }

        public bool isTotal() {
            return isSomething("Total");
        }

        public bool isTransmission() {
            return isSomething("Transmission");
        }

        public bool isTrigger() {
            return isSomething("Trigger");
        }

        public bool isUnits() {
            return isSomething("Units");
        }

        public bool isUnitsAndBuildings() {
            return isSomething("Units And Buildings");
        }

        public bool isUnmuteUnitSpeech() {
            return isSomething("Unmute Unit Speech");
        }

        public bool isUnpauseGame() {
            return isSomething("Unpause Game");
        }

        public bool isUnpauseTimer() {
            return isSomething("Unpause Timer");
        }

        public bool isVictory() {
            return isSomething("Victory");
        }

        public bool isWait() {
            return isSomething("Wait");
        }

    }
}
