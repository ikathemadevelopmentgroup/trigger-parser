﻿using System;
using System.Collections.Generic;
using TriggerParser.triggers;
using TriggerParser.triggers.actions;
using TriggerParser.triggers.conditions;

namespace TriggerParser {
    class Processor {

        private List<Combo> combos = new List<Combo>();

        public Processor(List<Trigger> triggers) {
            handleTriggers(triggers);
            foreach (Combo combo in combos) {
                Console.WriteLine(combo);
            }
        }

        private void hasSecretTrigger(Trigger trigger) {
            Combo combo = new Combo(trigger);
            List<Condition> conditions = trigger.getConditoins();
               foreach (Condition condition in conditions) {
                if (condition is ConditionBring) {
                    ConditionBring bring = condition as ConditionBring;
                    if (bring.getLocation().Equals("SECRET 4")) {
                           string unitName = bring.getUnitName();
                        int ammount = bring.getAmount();
                        combo.addInputUnit(ammount, unitName);
                    }
                }
            }
            List<triggers.actions.Action> actions = trigger.getActions();
            foreach (triggers.actions.Action action in actions) {
                if (action is ActionCreateUnit) {
                    ActionCreateUnit create = action as ActionCreateUnit;
                    if (create.getLocation().Equals("SECRET 4")) {
                        int ammount = create.getAmount();
                        string name = create.getUnitName();
                        combo.addOutputUnit(ammount, name);
                    }
                } else if (action is ActionCreateUnitWithProperties) {
                    ActionCreateUnitWithProperties create = action as ActionCreateUnitWithProperties;
                    if (create.getLocation().Equals("SECRET 4")) {
                        int ammount = create.getAmount();
                        string name = create.getUnitName();
                        combo.addOutputUnit(ammount, name);
                    }
                }
            }
            if (combo.finalize()) {
                combos.Add(combo);
            }
        }


        private void handleTriggers(List<Trigger> triggers) {
            foreach (Trigger trigger in triggers) {
                List<Condition> conditions = trigger.getConditoins();
                foreach (Condition condition in conditions) {
                    if (condition is ConditionBring) {
                        ConditionBring bring = condition as ConditionBring;
                        if (bring.getLocation().Equals("SECRET 4")) {
                            hasSecretTrigger(trigger);
                            break;
                        }
                    }
                }
            }
        }
        }
}
