# Notes #

* ~~Condition should be index-based as Actions~~
* ~~Rebase proc to generate class variables for each parameter, including getter and assignment~~
* ~~Please rewrite proc completely~~
* Consider parsing binary form
* Consider binary synthesis


# Links #
* [CHK Format](http://www.starcraftai.com/wiki/CHK_Format)
* [MoPaQ wiki](https://en.wikipedia.org/wiki/MPQ)
* [MoPaQ Format](https://wowdev.wiki/MPQ)


# Libraries #
* [crystalmpq](https://github.com/sgraf812/crystalmpq)
* [lawine](https://code.google.com/archive/p/lawine/)
* [mpqtool](https://github.com/WoW-Tools/MpqTool)


# Todo #
* ABNF for text form