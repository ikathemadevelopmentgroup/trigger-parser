﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriggerParser.triggers.conditions {

    class Condition : TriggerContent {

         protected Condition(Parser parser, int num): base(parser, num) { }

    }

    class ConditionAccumulate : Condition {

        private PlayerDef player;

        private Quantifier quantifier;

        private int amount;

        private Resources resources;

        public ConditionAccumulate(Parser parser) : base(parser, 4) {
            this.player = base.getPlayerDef(1);
            this.quantifier = base.getQuantifier(2);
            this.amount = base.getInt(3);
            this.resources = base.getResources(4);
        }

        public PlayerDef getPlayer() {
            return this.player;
        }

        public Quantifier getQuantifier() {
            return this.quantifier;
        }

        public int getAmount() {
            return this.amount;
        }

        public Resources getResources() {
            return this.resources;
        }
    }

    class ConditionAlways : Condition {

        public ConditionAlways(Parser parser) : base(parser, 0) {
        }
    }

    class ConditionBring : Condition {

        private PlayerDef player;

        private string unitName;

        private string location;

        private Quantifier quantifier;

        private int amount;

        public ConditionBring(Parser parser) : base(parser, 5) {
            this.player = base.getPlayerDef(1);
            this.unitName = base.getString(2);
            this.location = base.getString(3);
            this.quantifier = base.getQuantifier(4);
            this.amount = base.getInt(5);
        }

        public PlayerDef getPlayer() {
            return this.player;
        }

        public string getUnitName() {
            return this.unitName;
        }

        public string getLocation() {
            return this.location;
        }

        public Quantifier getQuantifier() {
            return this.quantifier;
        }

        public int getAmount() {
            return this.amount;
        }
    }

    class ConditionCommand : Condition {

        private PlayerDef player;

        private string unitName;

        private Quantifier quantifier;

        private int amount;

        public ConditionCommand(Parser parser) : base(parser, 4) {
            this.player = base.getPlayerDef(1);
            this.unitName = base.getString(2);
            this.quantifier = base.getQuantifier(3);
            this.amount = base.getInt(4);
        }

        public PlayerDef getPlayer() {
            return this.player;
        }

        public string getUnitName() {
            return this.unitName;
        }

        public Quantifier getQuantifier() {
            return this.quantifier;
        }

        public int getAmount() {
            return this.amount;
        }
    }

    class ConditionCommandTheLeast : Condition {

        private string unitName;

        public ConditionCommandTheLeast(Parser parser) : base(parser, 1) {
            this.unitName = base.getString(1);
        }

        public string getUnitName() {
            return this.unitName;
        }
    }

    class ConditionCommandTheLeastAt : Condition {

        private string unitName;

        private string location;

        public ConditionCommandTheLeastAt(Parser parser) : base(parser, 2) {
            this.unitName = base.getString(1);
            this.location = base.getString(2);
        }

        public string getUnitName() {
            return this.unitName;
        }

        public string getLocation() {
            return this.location;
        }
    }

    class ConditionCommandTheMost : Condition {

        private string unitName;

        public ConditionCommandTheMost(Parser parser) : base(parser, 1) {
            this.unitName = base.getString(1);
        }

        public string getUnitName() {
            return this.unitName;
        }
    }

    class ConditionCommandsTheMostAt : Condition {

        private string unitName;

        private string location;

        public ConditionCommandsTheMostAt(Parser parser) : base(parser, 2) {
            this.unitName = base.getString(1);
            this.location = base.getString(2);
        }

        public string getUnitName() {
            return this.unitName;
        }

        public string getLocation() {
            return this.location;
        }
    }

    class ConditionCountdownTimer : Condition {

        private Quantifier quantifier;

        private int amount;

        public ConditionCountdownTimer(Parser parser) : base(parser, 2) {
            this.quantifier = base.getQuantifier(1);
            this.amount = base.getInt(2);
        }

        public Quantifier getQuantifier() {
            return this.quantifier;
        }

        public int getAmount() {
            return this.amount;
        }
    }

    class ConditionDeaths : Condition {

        private PlayerDef player;

        private string unitName;

        private Quantifier quantifier;

        private int amount;

        public ConditionDeaths(Parser parser) : base(parser, 4) {
            this.player = base.getPlayerDef(1);
            this.unitName = base.getString(2);
            this.quantifier = base.getQuantifier(3);
            this.amount = base.getInt(4);
        }

        public PlayerDef getPlayer() {
            return this.player;
        }

        public string getUnitName() {
            return this.unitName;
        }

        public Quantifier getQuantifier() {
            return this.quantifier;
        }

        public int getAmount() {
            return this.amount;
        }
    }

    class ConditionElapsedTime : Condition {

        private Quantifier quantifier;

        private int amount;

        public ConditionElapsedTime(Parser parser) : base(parser, 2) {
            this.quantifier = base.getQuantifier(1);
            this.amount = base.getInt(2);
        }

        public Quantifier getQuantifier() {
            return this.quantifier;
        }

        public int getAmount() {
            return this.amount;
        }
    }

    class ConditionHighestScore : Condition {

        private ScoreBoard scoreBoard;

        public ConditionHighestScore(Parser parser) : base(parser, 1) {
            this.scoreBoard = base.getScoreBoard(1);
        }

        public ScoreBoard getScoreBoard() {
            return this.scoreBoard;
        }
    }

    class ConditionKill : Condition {

        private PlayerDef player;

        private string unitName;

        private Quantifier quantifier;

        private int amount;

        public ConditionKill(Parser parser) : base(parser, 4) {
            this.player = base.getPlayerDef(1);
            this.unitName = base.getString(2);
            this.quantifier = base.getQuantifier(3);
            this.amount = base.getInt(4);
        }

        public PlayerDef getPlayer() {
            return this.player;
        }

        public string getUnitName() {
            return this.unitName;
        }

        public Quantifier getQuantifier() {
            return this.quantifier;
        }

        public int getAmount() {
            return this.amount;
        }
    }

    class ConditionLeastKills : Condition {

        private string unitName;

        public ConditionLeastKills(Parser parser) : base(parser, 1) {
            this.unitName = base.getString(1);
        }

        public string getUnitName() {
            return this.unitName;
        }
    }

    class ConditionLeastResources : Condition {

        private Resources resources;

        public ConditionLeastResources(Parser parser) : base(parser, 1) {
            this.resources = base.getResources(1);
        }

        public Resources getResources() {
            return this.resources;
        }
    }

    class ConditionLowestScore : Condition {

        private ScoreBoard scoreBoard;

        public ConditionLowestScore(Parser parser) : base(parser, 1) {
            this.scoreBoard = base.getScoreBoard(1);
        }

        public ScoreBoard getScoreBoard() {
            return this.scoreBoard;
        }
    }

    class ConditionMostKills : Condition {

        private string unitName;

        public ConditionMostKills(Parser parser) : base(parser, 1) {
            this.unitName = base.getString(1);
        }

        public string getUnitName() {
            return this.unitName;
        }
    }

    class ConditionMostResources : Condition {

        private Resources resources;

        public ConditionMostResources(Parser parser) : base(parser, 1) {
            this.resources = base.getResources(1);
        }

        public Resources getResources() {
            return this.resources;
        }
    }

    class ConditionNever : Condition {

        public ConditionNever(Parser parser) : base(parser, 0) {
        }
    }

    class ConditionOpponents : Condition {

        private PlayerDef player;

        private Quantifier quantifier;

        private int amount;

        public ConditionOpponents(Parser parser) : base(parser, 3) {
            this.player = base.getPlayerDef(1);
            this.quantifier = base.getQuantifier(2);
            this.amount = base.getInt(3);
        }

        public PlayerDef getPlayer() {
            return this.player;
        }

        public Quantifier getQuantifier() {
            return this.quantifier;
        }

        public int getAmount() {
            return this.amount;
        }
    }

    class ConditionScore : Condition {

        private PlayerDef player;

        private ScoreBoard scoreBoard;

        private Quantifier quantifier;

        private int amount;

        public ConditionScore(Parser parser) : base(parser, 4) {
            this.player = base.getPlayerDef(1);
            this.scoreBoard = base.getScoreBoard(2);
            this.quantifier = base.getQuantifier(3);
            this.amount = base.getInt(4);
        }

        public PlayerDef getPlayer() {
            return this.player;
        }

        public ScoreBoard getScoreBoard() {
            return this.scoreBoard;
        }

        public Quantifier getQuantifier() {
            return this.quantifier;
        }

        public int getAmount() {
            return this.amount;
        }
    }

    class ConditionSwitch : Condition {

        private PlayerDef player;

        private SwitchState switchState;

        public ConditionSwitch(Parser parser) : base(parser, 2) {
            this.player = base.getPlayerDef(1);
            this.switchState = base.getSwitchState(2);
        }

        public PlayerDef getPlayer() {
            return this.player;
        }

        public SwitchState getSwitchState() {
            return this.switchState;
        }
    }

}
