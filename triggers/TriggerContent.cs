﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriggerParser.triggers {
    class TriggerContent {

        private bool enabled = false;

        public bool isEnabled() {
            return enabled;
        }

        public void setEnabled(bool enabled) {
            this.enabled = enabled;
        }

        private Token[] tokens;

        protected TriggerContent(Parser sc, int num) {
            tokens = new Token[num];
            if (!(sc.getNextToken() is LeftBracket)) {
                throw new NotImplementedException();
            }
            for (int i = 0; i < num; i++) {
                tokens[i] = sc.getNextToken();
                if (i + 1 < num) {
                    if (!(sc.getNextToken() is Comma)) {
                        throw new NotImplementedException();
                    }
                }
            }
            if (!(sc.getNextToken() is RightBracket)) {
                throw new NotImplementedException();
            }
        }

        protected EnableState getEnableState(int index) {
            return tokens[index - 1].toEnableState();
        }

        protected SwitchSetState getSwitchSetState(int index) {
            return tokens[index - 1].toSwitchSetState();
        }

        protected MessageType getMessageType(int index) {
            return tokens[index - 1].getMessageType();
        }

        protected string getString(int index) {
            return tokens[index - 1].getContent();
        }

        protected int getInt(int index) {
            return tokens[index - 1].toInt();
        }

        protected Quantifier getQuantifier(int index) {
            return tokens[index - 1].toQuantifier();
        }

        protected Order getOrder(int index) {
            return tokens[index - 1].toOrder();
        }

        protected SetQuantifier getSetQuantifier(int index) {
            return tokens[index - 1].toSetQuantifier();
        }

        protected PlayerDef getPlayerDef(int index) {
            return tokens[index - 1].toPlayerDef();
        }

        protected Resources getResources(int index) {
            return tokens[index - 1].toResources();
        }

        protected ScoreBoard getScoreBoard(int index) {
            return tokens[index - 1].toScoreBoard();
        }

        protected Alliance getAlliance(int index) {
            return tokens[index - 1].toAlliance();
        }

        protected SwitchState getSwitchState(int index) {
            return tokens[index - 1].toSwitchState();
        }


    }
}
