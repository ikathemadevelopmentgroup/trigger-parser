﻿namespace TriggerParser.triggers.actions {

    class Action: TriggerContent {

        public Action(Parser parser, int count) : base(parser, count) { }

    }

    class ActionCenterView : Action {

        private string location;

        public ActionCenterView(Parser parser) : base(parser, 1) {
            this.location = base.getString(1);
        }

        public string getLocation() {
            return this.location;
        }
    }

    class ActionComment : Action {

        private string title;

        public ActionComment(Parser parser) : base(parser, 1) {
            this.title = base.getString(1);
        }

        public string getTitle() {
            return this.title;
        }
    }

    class ActionCreateUnit : Action {

        private PlayerDef player;

        private string unitName;

        private int amount;

        private string location;

        public ActionCreateUnit(Parser parser) : base(parser, 4) {
            this.player = base.getPlayerDef(1);
            this.unitName = base.getString(2);
            this.amount = base.getInt(3);
            this.location = base.getString(4);
        }

        public PlayerDef getPlayer() {
            return this.player;
        }

        public string getUnitName() {
            return this.unitName;
        }

        public int getAmount() {
            return this.amount;
        }

        public string getLocation() {
            return this.location;
        }
    }

    class ActionCreateUnitWithProperties : Action {

        private PlayerDef player;

        private string unitName;

        private int amount;

        private string location;

        private int properties;

        public ActionCreateUnitWithProperties(Parser parser) : base(parser, 5) {
            this.player = base.getPlayerDef(1);
            this.unitName = base.getString(2);
            this.amount = base.getInt(3);
            this.location = base.getString(4);
            this.properties = base.getInt(5);
        }

        public PlayerDef getPlayer() {
            return this.player;
        }

        public string getUnitName() {
            return this.unitName;
        }

        public int getAmount() {
            return this.amount;
        }

        public string getLocation() {
            return this.location;
        }

        public int getProperties() {
            return this.properties;
        }
    }

    class ActionDefeat : Action {

        public ActionDefeat(Parser parser) : base(parser, 0) {
        }
    }

    class ActionDisplayTextMessage : Action {

        private MessageType messageType;

        private string message;

        public ActionDisplayTextMessage(Parser parser) : base(parser, 2) {
            this.messageType = base.getMessageType(1);
            this.message = base.getString(2);
        }

        public MessageType getMessageType() {
            return this.messageType;
        }

        public string getMessage() {
            return this.message;
        }
    }

    class ActionDraw : Action {

        public ActionDraw(Parser parser) : base(parser, 0) {
        }
    }

    class ActionGiveUnitsToPlayer : Action {

        private PlayerDef sourcePlayer;

        private PlayerDef targetPlayer;

        private string unitName;

        private int amount;

        private string location;

        public ActionGiveUnitsToPlayer(Parser parser) : base(parser, 5) {
            this.sourcePlayer = base.getPlayerDef(1);
            this.targetPlayer = base.getPlayerDef(2);
            this.unitName = base.getString(3);
            this.amount = base.getInt(4);
            this.location = base.getString(5);
        }

        public PlayerDef getSourcePlayer() {
            return this.sourcePlayer;
        }

        public PlayerDef getTargetPlayer() {
            return this.targetPlayer;
        }

        public string getUnitName() {
            return this.unitName;
        }

        public int getAmount() {
            return this.amount;
        }

        public string getLocation() {
            return this.location;
        }
    }

    class ActionKillUnit : Action {

        private PlayerDef player;

        private string unitName;

        public ActionKillUnit(Parser parser) : base(parser, 2) {
            this.player = base.getPlayerDef(1);
            this.unitName = base.getString(2);
        }

        public PlayerDef getPlayer() {
            return this.player;
        }

        public string getUnitName() {
            return this.unitName;
        }
    }

    class ActionKillUnitAtLocation : Action {

        private PlayerDef player;

        private string unitName;

        private int amount;

        private string location;

        public ActionKillUnitAtLocation(Parser parser) : base(parser, 4) {
            this.player = base.getPlayerDef(1);
            this.unitName = base.getString(2);
            this.amount = base.getInt(3);
            this.location = base.getString(4);
        }

        public PlayerDef getPlayer() {
            return this.player;
        }

        public string getUnitName() {
            return this.unitName;
        }

        public int getAmount() {
            return this.amount;
        }

        public string getLocation() {
            return this.location;
        }
    }

    class ActionLeaderBoardControlAtLocation : Action {

        private string title;

        private string unitName;

        private string location;

        public ActionLeaderBoardControlAtLocation(Parser parser) : base(parser, 3) {
            this.title = base.getString(1);
            this.unitName = base.getString(2);
            this.location = base.getString(3);
        }

        public string getTitle() {
            return this.title;
        }

        public string getUnitName() {
            return this.unitName;
        }

        public string getLocation() {
            return this.location;
        }
    }

    class ActionLeaderBoardControl : Action {

        private string title;

        private string unitName;

        public ActionLeaderBoardControl(Parser parser) : base(parser, 2) {
            this.title = base.getString(1);
            this.unitName = base.getString(2);
        }

        public string getTitle() {
            return this.title;
        }

        public string getUnitName() {
            return this.unitName;
        }
    }

    class ActionLeaderboardGreed : Action {

        private int amount;

        public ActionLeaderboardGreed(Parser parser) : base(parser, 1) {
            this.amount = base.getInt(1);
        }

        public int getAmount() {
            return this.amount;
        }
    }

    class ActionLeaderBoardKills : Action {

        private string title;

        private string unitName;

        public ActionLeaderBoardKills(Parser parser) : base(parser, 2) {
            this.title = base.getString(1);
            this.unitName = base.getString(2);
        }

        public string getTitle() {
            return this.title;
        }

        public string getUnitName() {
            return this.unitName;
        }
    }

    class ActionLeaderBoardPoints : Action {

        private string title;

        private ScoreBoard scoreBoard;

        public ActionLeaderBoardPoints(Parser parser) : base(parser, 2) {
            this.title = base.getString(1);
            this.scoreBoard = base.getScoreBoard(2);
        }

        public string getTitle() {
            return this.title;
        }

        public ScoreBoard getScoreBoard() {
            return this.scoreBoard;
        }
    }

    class ActionLeaderBoardResources : Action {

        private string title;

        private Resources resources;

        public ActionLeaderBoardResources(Parser parser) : base(parser, 2) {
            this.title = base.getString(1);
            this.resources = base.getResources(2);
        }

        public string getTitle() {
            return this.title;
        }

        public Resources getResources() {
            return this.resources;
        }
    }

    class ActionLeaderboardComputerPlayers : Action {

        private EnableState state;

        public ActionLeaderboardComputerPlayers(Parser parser) : base(parser, 1) {
            this.state = base.getEnableState(1);
        }

        public EnableState getState() {
            return this.state;
        }
    }

    class ActionLeaderboardGoalControlAtLocation : Action {

        private string title;

        private string unitName;

        private int amount;

        private string location;

        public ActionLeaderboardGoalControlAtLocation(Parser parser) : base(parser, 4) {
            this.title = base.getString(1);
            this.unitName = base.getString(2);
            this.amount = base.getInt(3);
            this.location = base.getString(4);
        }

        public string getTitle() {
            return this.title;
        }

        public string getUnitName() {
            return this.unitName;
        }

        public int getAmount() {
            return this.amount;
        }

        public string getLocation() {
            return this.location;
        }
    }

    class ActionLeaderboardGoalControl : Action {

        private string title;

        private string unitName;

        private int amount;

        public ActionLeaderboardGoalControl(Parser parser) : base(parser, 3) {
            this.title = base.getString(1);
            this.unitName = base.getString(2);
            this.amount = base.getInt(3);
        }

        public string getTitle() {
            return this.title;
        }

        public string getUnitName() {
            return this.unitName;
        }

        public int getAmount() {
            return this.amount;
        }
    }

    class ActionLeaderboardGoalKills : Action {

        private string title;

        private string unitName;

        private int amount;

        public ActionLeaderboardGoalKills(Parser parser) : base(parser, 3) {
            this.title = base.getString(1);
            this.unitName = base.getString(2);
            this.amount = base.getInt(3);
        }

        public string getTitle() {
            return this.title;
        }

        public string getUnitName() {
            return this.unitName;
        }

        public int getAmount() {
            return this.amount;
        }
    }

    class ActionLeaderboardGoalPoints : Action {

        private string title;

        private ScoreBoard scoreBoard;

        private int amount;

        public ActionLeaderboardGoalPoints(Parser parser) : base(parser, 3) {
            this.title = base.getString(1);
            this.scoreBoard = base.getScoreBoard(2);
            this.amount = base.getInt(3);
        }

        public string getTitle() {
            return this.title;
        }

        public ScoreBoard getScoreBoard() {
            return this.scoreBoard;
        }

        public int getAmount() {
            return this.amount;
        }
    }

    class ActionLeaderboardGoalResources : Action {

        private string title;

        private int amount;

        private Resources resources;

        public ActionLeaderboardGoalResources(Parser parser) : base(parser, 3) {
            this.title = base.getString(1);
            this.amount = base.getInt(2);
            this.resources = base.getResources(3);
        }

        public string getTitle() {
            return this.title;
        }

        public int getAmount() {
            return this.amount;
        }

        public Resources getResources() {
            return this.resources;
        }
    }

    class ActionMinimapPing : Action {

        private string location;

        public ActionMinimapPing(Parser parser) : base(parser, 1) {
            this.location = base.getString(1);
        }

        public string getLocation() {
            return this.location;
        }
    }

    class ActionModifyUnitEnergy : Action {

        private PlayerDef player;

        private string unitName;

        private int percentage;

        private int amount;

        private string location;

        public ActionModifyUnitEnergy(Parser parser) : base(parser, 5) {
            this.player = base.getPlayerDef(1);
            this.unitName = base.getString(2);
            this.percentage = base.getInt(3);
            this.amount = base.getInt(4);
            this.location = base.getString(5);
        }

        public PlayerDef getPlayer() {
            return this.player;
        }

        public string getUnitName() {
            return this.unitName;
        }

        public int getPercentage() {
            return this.percentage;
        }

        public int getAmount() {
            return this.amount;
        }

        public string getLocation() {
            return this.location;
        }
    }

    class ActionModifyUnitHangerCount : Action {

        private PlayerDef player;

        private string unitName;

        private int newHangarCount;

        private int amount;

        private string location;

        public ActionModifyUnitHangerCount(Parser parser) : base(parser, 5) {
            this.player = base.getPlayerDef(1);
            this.unitName = base.getString(2);
            this.newHangarCount = base.getInt(3);
            this.amount = base.getInt(4);
            this.location = base.getString(5);
        }

        public PlayerDef getPlayer() {
            return this.player;
        }

        public string getUnitName() {
            return this.unitName;
        }

        public int getNewHangarCount() {
            return this.newHangarCount;
        }

        public int getAmount() {
            return this.amount;
        }

        public string getLocation() {
            return this.location;
        }
    }

    class ActionModifyUnitHitPoints : Action {

        private PlayerDef player;

        private string unitName;

        private int newHitPoint;

        private int amount;

        private string location;

        public ActionModifyUnitHitPoints(Parser parser) : base(parser, 5) {
            this.player = base.getPlayerDef(1);
            this.unitName = base.getString(2);
            this.newHitPoint = base.getInt(3);
            this.amount = base.getInt(4);
            this.location = base.getString(5);
        }

        public PlayerDef getPlayer() {
            return this.player;
        }

        public string getUnitName() {
            return this.unitName;
        }

        public int getNewHitPoint() {
            return this.newHitPoint;
        }

        public int getAmount() {
            return this.amount;
        }

        public string getLocation() {
            return this.location;
        }
    }

    class ActionModifyUnitResourceAmount : Action {

        private PlayerDef player;

        private int percentage;

        private int amount;

        private string location;

        public ActionModifyUnitResourceAmount(Parser parser) : base(parser, 4) {
            this.player = base.getPlayerDef(1);
            this.percentage = base.getInt(2);
            this.amount = base.getInt(3);
            this.location = base.getString(4);
        }

        public PlayerDef getPlayer() {
            return this.player;
        }

        public int getPercentage() {
            return this.percentage;
        }

        public int getAmount() {
            return this.amount;
        }

        public string getLocation() {
            return this.location;
        }
    }

    class ActionModifyUnitShieldPoints : Action {

        private PlayerDef player;

        private string unitName;

        private int percentage;

        private int amount;

        private string location;

        public ActionModifyUnitShieldPoints(Parser parser) : base(parser, 5) {
            this.player = base.getPlayerDef(1);
            this.unitName = base.getString(2);
            this.percentage = base.getInt(3);
            this.amount = base.getInt(4);
            this.location = base.getString(5);
        }

        public PlayerDef getPlayer() {
            return this.player;
        }

        public string getUnitName() {
            return this.unitName;
        }

        public int getPercentage() {
            return this.percentage;
        }

        public int getAmount() {
            return this.amount;
        }

        public string getLocation() {
            return this.location;
        }
    }

    class ActionMoveLocation : Action {

        private PlayerDef player;

        private string unitName;

        private string sourceLocation;

        private string targetLocation;

        public ActionMoveLocation(Parser parser) : base(parser, 4) {
            this.player = base.getPlayerDef(1);
            this.unitName = base.getString(2);
            this.sourceLocation = base.getString(3);
            this.targetLocation = base.getString(4);
        }

        public PlayerDef getPlayer() {
            return this.player;
        }

        public string getUnitName() {
            return this.unitName;
        }

        public string getSourceLocation() {
            return this.sourceLocation;
        }

        public string getTargetLocation() {
            return this.targetLocation;
        }
    }

    class ActionPlayWAV : Action {

        private string wavPath;

        private string parameter;

        public ActionPlayWAV(Parser parser) : base(parser, 2) {
            this.wavPath = base.getString(1);
            this.parameter = base.getString(2);
        }

        public string getWavPath() {
            return this.wavPath;
        }

        public string getParameter() {
            return this.parameter;
        }
    }

    class ActionMoveUnit : Action {

        private PlayerDef player;

        private string unitName;

        private int amount;

        private string sourceLocation;

        private string targetLocation;

        public ActionMoveUnit(Parser parser) : base(parser, 5) {
            this.player = base.getPlayerDef(1);
            this.unitName = base.getString(2);
            this.amount = base.getInt(3);
            this.sourceLocation = base.getString(4);
            this.targetLocation = base.getString(5);
        }

        public PlayerDef getPlayer() {
            return this.player;
        }

        public string getUnitName() {
            return this.unitName;
        }

        public int getAmount() {
            return this.amount;
        }

        public string getSourceLocation() {
            return this.sourceLocation;
        }

        public string getTargetLocation() {
            return this.targetLocation;
        }
    }

    class ActionMuteUnitSpeech : Action {

        public ActionMuteUnitSpeech(Parser parser) : base(parser, 0) {
        }
    }

    class ActionOrder : Action {

        private PlayerDef player;

        private string unitName;

        private string sourceLocation;

        private string targetLocation;

        private Order orderType;

        public ActionOrder(Parser parser) : base(parser, 5) {
            this.player = base.getPlayerDef(1);
            this.unitName = base.getString(2);
            this.sourceLocation = base.getString(3);
            this.targetLocation = base.getString(4);
            this.orderType = base.getOrder(5);
        }

        public PlayerDef getPlayer() {
            return this.player;
        }

        public string getUnitName() {
            return this.unitName;
        }

        public string getSourceLocation() {
            return this.sourceLocation;
        }

        public string getTargetLocation() {
            return this.targetLocation;
        }

        public Order getOrderType() {
            return this.orderType;
        }
    }

    class ActionPauseGame : Action {

        public ActionPauseGame(Parser parser) : base(parser, 0) {
        }
    }

    class ActionPauseTimer : Action {

        public ActionPauseTimer(Parser parser) : base(parser, 0) {
        }
    }

    class ActionPreserveTrigger : Action {

        public ActionPreserveTrigger(Parser parser) : base(parser, 0) {
        }
    }

    class ActionRemoveUnit : Action {

        private PlayerDef player;

        private string unitName;

        public ActionRemoveUnit(Parser parser) : base(parser, 2) {
            this.player = base.getPlayerDef(1);
            this.unitName = base.getString(2);
        }

        public PlayerDef getPlayer() {
            return this.player;
        }

        public string getUnitName() {
            return this.unitName;
        }
    }

    class ActionRemoveUnitAtLocation : Action {

        private PlayerDef player;

        private string unitName;

        private int amount;

        private string location;

        public ActionRemoveUnitAtLocation(Parser parser) : base(parser, 4) {
            this.player = base.getPlayerDef(1);
            this.unitName = base.getString(2);
            this.amount = base.getInt(3);
            this.location = base.getString(4);
        }

        public PlayerDef getPlayer() {
            return this.player;
        }

        public string getUnitName() {
            return this.unitName;
        }

        public int getAmount() {
            return this.amount;
        }

        public string getLocation() {
            return this.location;
        }
    }

    class ActionRunAIScript : Action {

        private string scriptName;

        public ActionRunAIScript(Parser parser) : base(parser, 1) {
            this.scriptName = base.getString(1);
        }

        public string getScriptName() {
            return this.scriptName;
        }
    }

    class ActionRunAIScriptAtLocation : Action {

        private string scriptName;

        private string location;

        public ActionRunAIScriptAtLocation(Parser parser) : base(parser, 2) {
            this.scriptName = base.getString(1);
            this.location = base.getString(2);
        }

        public string getScriptName() {
            return this.scriptName;
        }

        public string getLocation() {
            return this.location;
        }
    }

    class ActionSetAllianceStatus : Action {

        private PlayerDef player;

        private Alliance alliance;

        public ActionSetAllianceStatus(Parser parser) : base(parser, 2) {
            this.player = base.getPlayerDef(1);
            this.alliance = base.getAlliance(2);
        }

        public PlayerDef getPlayer() {
            return this.player;
        }

        public Alliance getAlliance() {
            return this.alliance;
        }
    }

    class ActionSetCountdownTimer : Action {

        private SetQuantifier setQuantifier;

        private int amount;

        public ActionSetCountdownTimer(Parser parser) : base(parser, 2) {
            this.setQuantifier = base.getSetQuantifier(1);
            this.amount = base.getInt(2);
        }

        public SetQuantifier getSetQuantifier() {
            return this.setQuantifier;
        }

        public int getAmount() {
            return this.amount;
        }
    }

    class ActionSetDeaths : Action {

        private PlayerDef player;

        private string unitName;

        private SetQuantifier setQuantifier;

        private int amount;

        public ActionSetDeaths(Parser parser) : base(parser, 4) {
            this.player = base.getPlayerDef(1);
            this.unitName = base.getString(2);
            this.setQuantifier = base.getSetQuantifier(3);
            this.amount = base.getInt(4);
        }

        public PlayerDef getPlayer() {
            return this.player;
        }

        public string getUnitName() {
            return this.unitName;
        }

        public SetQuantifier getSetQuantifier() {
            return this.setQuantifier;
        }

        public int getAmount() {
            return this.amount;
        }
    }

    class ActionSetDoodadState : Action {

        private PlayerDef player;

        private string unitName;

        private string location;

        private EnableState state;

        public ActionSetDoodadState(Parser parser) : base(parser, 4) {
            this.player = base.getPlayerDef(1);
            this.unitName = base.getString(2);
            this.location = base.getString(3);
            this.state = base.getEnableState(4);
        }

        public PlayerDef getPlayer() {
            return this.player;
        }

        public string getUnitName() {
            return this.unitName;
        }

        public string getLocation() {
            return this.location;
        }

        public EnableState getState() {
            return this.state;
        }
    }

    class ActionSetInvincibility : Action {

        private PlayerDef player;

        private string unitName;

        private string location;

        private EnableState state;

        public ActionSetInvincibility(Parser parser) : base(parser, 4) {
            this.player = base.getPlayerDef(1);
            this.unitName = base.getString(2);
            this.location = base.getString(3);
            this.state = base.getEnableState(4);
        }

        public PlayerDef getPlayer() {
            return this.player;
        }

        public string getUnitName() {
            return this.unitName;
        }

        public string getLocation() {
            return this.location;
        }

        public EnableState getState() {
            return this.state;
        }
    }

    class ActionSetMissionObjectives : Action {

        private string newObjective;

        public ActionSetMissionObjectives(Parser parser) : base(parser, 1) {
            this.newObjective = base.getString(1);
        }

        public string getNewObjective() {
            return this.newObjective;
        }
    }

    class ActionSetNextScenario : Action {

        private string nextScenario;

        public ActionSetNextScenario(Parser parser) : base(parser, 1) {
            this.nextScenario = base.getString(1);
        }

        public string getNextScenario() {
            return this.nextScenario;
        }
    }

    class ActionSetResources : Action {

        private PlayerDef player;

        private SetQuantifier setQuantifier;

        private int amount;

        private Resources resources;

        public ActionSetResources(Parser parser) : base(parser, 4) {
            this.player = base.getPlayerDef(1);
            this.setQuantifier = base.getSetQuantifier(2);
            this.amount = base.getInt(3);
            this.resources = base.getResources(4);
        }

        public PlayerDef getPlayer() {
            return this.player;
        }

        public SetQuantifier getSetQuantifier() {
            return this.setQuantifier;
        }

        public int getAmount() {
            return this.amount;
        }

        public Resources getResources() {
            return this.resources;
        }
    }

    class ActionSetScore : Action {

        private PlayerDef player;

        private SetQuantifier setQuantifier;

        private int amount;

        private ScoreBoard scoreBoard;

        public ActionSetScore(Parser parser) : base(parser, 4) {
            this.player = base.getPlayerDef(1);
            this.setQuantifier = base.getSetQuantifier(2);
            this.amount = base.getInt(3);
            this.scoreBoard = base.getScoreBoard(4);
        }

        public PlayerDef getPlayer() {
            return this.player;
        }

        public SetQuantifier getSetQuantifier() {
            return this.setQuantifier;
        }

        public int getAmount() {
            return this.amount;
        }

        public ScoreBoard getScoreBoard() {
            return this.scoreBoard;
        }
    }

    class ActionSetSwitch : Action {

        private string switchName;

        private SwitchSetState switchSetState;

        public ActionSetSwitch(Parser parser) : base(parser, 2) {
            this.switchName = base.getString(1);
            this.switchSetState = base.getSwitchSetState(2);
        }

        public string getSwitchName() {
            return this.switchName;
        }

        public SwitchSetState getSwitchSetState() {
            return this.switchSetState;
        }
    }

    class ActionTalkingPortrait : Action {

        private string unitName;

        private int timeout;

        public ActionTalkingPortrait(Parser parser) : base(parser, 2) {
            this.unitName = base.getString(1);
            this.timeout = base.getInt(2);
        }

        public string getUnitName() {
            return this.unitName;
        }

        public int getTimeout() {
            return this.timeout;
        }
    }

    class ActionTransmission : Action {

        private MessageType messageType;

        private string title;

        private string unitName;

        private string location;

        private SetQuantifier setQuantifier;

        private int amount;

        private string wavPath;

        private int duration;

        public ActionTransmission(Parser parser) : base(parser, 8) {
            this.messageType = base.getMessageType(1);
            this.title = base.getString(2);
            this.unitName = base.getString(3);
            this.location = base.getString(4);
            this.setQuantifier = base.getSetQuantifier(5);
            this.amount = base.getInt(6);
            this.wavPath = base.getString(7);
            this.duration = base.getInt(8);
        }

        public MessageType getMessageType() {
            return this.messageType;
        }

        public string getTitle() {
            return this.title;
        }

        public string getUnitName() {
            return this.unitName;
        }

        public string getLocation() {
            return this.location;
        }

        public SetQuantifier getSetQuantifier() {
            return this.setQuantifier;
        }

        public int getAmount() {
            return this.amount;
        }

        public string getWavPath() {
            return this.wavPath;
        }

        public int getDuration() {
            return this.duration;
        }
    }

    class ActionUnmuteUnitSpeech : Action {

        public ActionUnmuteUnitSpeech(Parser parser) : base(parser, 0) {
        }
    }

    class ActionUnpauseGame : Action {

        public ActionUnpauseGame(Parser parser) : base(parser, 0) {
        }
    }

    class ActionUnpauseTimer : Action {

        public ActionUnpauseTimer(Parser parser) : base(parser, 0) {
        }
    }

    class ActionVictory : Action {

        public ActionVictory(Parser parser) : base(parser, 0) {
        }
    }

    class ActionWait : Action {

        private int timeout;

        public ActionWait(Parser parser) : base(parser, 1) {
            this.timeout = base.getInt(1);
        }

        public int getTimeout() {
            return this.timeout;
        }
    }

}
