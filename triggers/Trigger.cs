﻿using System.Collections.Generic;
using TriggerParser.triggers.conditions;
using TriggerParser.triggers.actions;
using System;
using System.Text;

namespace TriggerParser.triggers {
    class Trigger {
        private Token affectName;

        private List<Condition> conditions = new List<Condition>();
        private List<actions.Action> actions = new List<actions.Action>();

        public Trigger() {
            
        }

        public void setAffectName(Token affectName) {
            this.affectName = affectName;
        }

        public void addCondition(Condition c) {
            conditions.Add(c);
        }

        public void addAction(actions.Action a) {
            actions.Add(a);
        }

        public List<actions.Action> getActions() {
            return actions;
        }

        private StringBuilder sb = new StringBuilder();

        internal void addToken(Token t) {
            if (t != null) {
                sb.Append(t.getContent());
            }
        }

        public override string ToString() {
            return sb.ToString();
        }

        internal List<Condition> getConditoins() {
            return conditions;
        }
    }

    public enum Alliance {
        Ally,
        Enemy,
        AlliedVictory
    }

    public enum SwitchSetState {
        Set,
        Clear,
        Randomize,
        Toggle
    }

    public enum EnableState {
        Enable,
        Disable,
        Toggle
    }

    public enum SetQuantifier {
        Add,
        Subtract,
        SetTo
    }

    public enum Order {
        Attack,
        Move,
        Patrol
    }

    public enum MessageType {
        ALwaysDisplay,
        DontAlwaysDisplay
    }

    public enum SwitchState {
        Set,
        NotSet
    }

    public enum Quantifier {
        AtMost,
        AtLeast,
        Exactly
    }

    public enum Resources {
        Ore,
        Gas,
        OreAndGas
    }

    public enum ScoreBoard {
        Buildings,
        Custom,
        Kills,
        KillsAndRazings,
        Razings,
        Total,
        Units,
        UnitsAndBuildings
    }

    public enum PlayerDef {
        AllPlayers,
        Allies,
        CurrentPlayer,
        Foes,
        Force1,
        Force2,
        Force3,
        Force4,
        Neutral,
        NeutralPlayers,
        NonAlliedVictoryPlayers,
        Player1,
        Player2,
        Player3,
        Player4,
        Player5,
        Player6,
        Player7,
        Player8,
        Player9,
        Player10,
        Player11,
        UnknownUnused
    }
}
