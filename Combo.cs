﻿using System;
using System.Collections.Generic;
using TriggerParser.triggers;

namespace TriggerParser {
    class Combo {

        internal class ComboItem {
            public int howMany;
            public string unitName;

            public override string ToString() {
                return howMany + " of " + unitName;
            }
        }

        private Trigger trigger;

        public Combo(Trigger sourceTrigger) {
            this.trigger = sourceTrigger;
        }

        List<ComboItem> input = new List<ComboItem>();

        List<ComboItem> output = new List<ComboItem>();

        public override string ToString() {
            string inp = string.Join(" + ", input);
            string outp = string.Join(" + ", output);
            return inp + " => " + outp;
        }


        public void addInputUnit(int howMany, string unitName) {
            ComboItem str = new ComboItem();
            str.howMany = howMany;
            str.unitName = unitName;
            input.Add(str);
        }

        public void addOutputUnit(int howMany, string unitName) {
            ComboItem str = new ComboItem();
            str.howMany = howMany;
            str.unitName = unitName;
            output.Add(str);
        }

        public bool finalize() {
            if (input.Count == 0 && output.Count > 0) {
                return false;
            } else if (output.Count == 0 && input.Count > 0) {
                return false;
            } else {
                return true;
            }
        }
    }
}
