﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriggerParser {
    class Scanner {
        private string v;
        StreamReader reader;

        public Scanner(string v) {
            this.v = v;
            reader = new StreamReader(v);
        }

        public void close() {
            reader.Close();
        }

        private char lastChar;
        private bool hasLastChar = false;

        private char getNextChar() {
            if (hasLastChar) {
                hasLastChar = false;
            } else {
                lastChar = (char)reader.Read();
            }
            return lastChar;
        }

        private void unreadLastChar() {
            hasLastChar = true;
        }

        public Token getNextToken() {
            char ch = getNextChar();
            switch(ch) {
                case '"':
                    return getString();
                case '(':
                    return new LeftBracket();
                case ')':
                    return new RightBracket();
                case ':':
                    return new Colon();
                case '.':
                    return new Dot();
                case ',':
                    return new Comma();
                case ';':
                    return new Semicolon();
                case '/':
                    unreadLastChar();
                    return getTokenEnd();
                case ' ':
                case '\t':
                case '\r':
                case '\n':
                    return getNextToken();
                case '{':
                    return new StartBracket();
                case '}':
                    return new EndBracket();
            }
            if(ch >= '0' && ch <= '9') {
                unreadLastChar();
                return getNumber();
            }
            if((ch >='A' && ch<='Z') || (ch >= 'a' && ch <= 'z')) {
                unreadLastChar();
                return getCommandToken();
            }
            return null;
        }

        private Token getCommandToken() {
            StringBuilder sb = new StringBuilder();
            while (true) {
                char ch = getNextChar();
                if ((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z') ||ch == ' ' ||ch=='\'') {
                    sb.Append(ch);
                } else {
                    unreadLastChar();
                    CommandToken t = new CommandToken(sb.ToString());
                    if(t.isValid()) {
                        return t;
                    } else { // Invalid trigger?
                        return t;
                    }
                }
            }
        }

        private Token getTokenEnd() {
            int count = 0;
            while (true) {
                char ch = getNextChar();
                if (ch == '/') {
                    count++;
                    if (count == 4) {
                        return new TokenEnd();
                    }
                }
            }
        }

        private Token getNumber() {
            StringBuilder sb = new StringBuilder();
            while(true) {
                char ch = getNextChar();
                if(ch >= '0' && ch <= '9') {
                    sb.Append(ch);
                } else {
                    unreadLastChar();
                    return new NumToken(sb.ToString());
                }
            }
        }

        private Token getString() {
            StringBuilder sb = new StringBuilder();
            while (true) {
                char ch = getNextChar();
                if (ch != '"') {
                    sb.Append(ch);
                } else {
                    return new StringToken(sb.ToString());
                }
            }
        }
    }
}
