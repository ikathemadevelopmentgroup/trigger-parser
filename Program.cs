﻿using System;
using System.Collections.Generic;
using TriggerParser.chk;

namespace TriggerParser {
    class Program {

        private static void parseText() {
            Scanner s = new Scanner("Triggers.txt");
            Parser p = new Parser(s);
            if (p.parse()) {
                p.close();
                new Processor(p.getTriggers());
            } else {
                Console.WriteLine("Parse failed");
                p.close();
            }
            Console.WriteLine("The End");
            Console.ReadLine();
        }

        private static void parseBinary() {
            List<Section> lst = CHKFile.getSections("scenario.chk");
            foreach(Section s in lst) {
                //;
            }
        }

        public static void Main(string[] args) {
            parseBinary();
        }
    }
}
